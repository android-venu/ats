package com.hackernews.newslist;

import com.google.common.collect.Lists;
import com.hackernews.model.NewsInfo;
import com.hackernews.newslist.NewsListContract.NewsInteractor;
import com.hackernews.newslist.NewsListContract.NewsListView;
import com.hackernews.newslist.NewsListContract.NewsInteractor.OnFinishedListener;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NewsListPresenterTest {

    private static List<NewsInfo> NEWS_LIST = Lists.newArrayList(new NewsInfo("Person1", "Title1",new long[]{1,2,3}),
            new NewsInfo("Person2", "Title2",new long[]{1,2,3}));

    private static List<NewsInfo> EMPTY_NEWS = new ArrayList<>(0);

    @Captor
    private ArgumentCaptor<OnFinishedListener> mLoadNewsCallbackCaptor;

    @Mock
    NewsInteractor mInteractor;
    @Mock
    NewsListView mView;
    @Mock
    OnFinishedListener mListener;
    @Mock
    NewsInfo mNewsInfo;

    NewsListContract.UserActionsListener mUserActionListener;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mUserActionListener = new NewsListPresenter(mView, mInteractor);
    }

    @Test
    public void onDestroy() {
        mUserActionListener.onDestroy();
        assertEquals(true, mUserActionListener.isViewReleased());
    }

    // Test using captor
    @Test
    public void onPullDownToRefresh_onFinished(){
        mUserActionListener.onPullDownToRefresh();
        verify(mView).showProgress();

        // Callback is captured and invoked with stubbed news items
        verify(mInteractor).getNewsArrayList(mLoadNewsCallbackCaptor.capture());
        mLoadNewsCallbackCaptor.getValue().onFinished(NEWS_LIST);
        verify(mView).setDataToRecyclerView(NEWS_LIST);
        verify(mView).hideProgress();
    }

    // Test using captor
    @Test
    public void onPullDownToRefresh_onFailure(){
        Throwable t = new IOException();
        mUserActionListener.onPullDownToRefresh();
        verify(mView).showProgress();

        // Callback is captured and invoked with stubbed news items
        verify(mInteractor).getNewsArrayList(mLoadNewsCallbackCaptor.capture());
        mLoadNewsCallbackCaptor.getValue().onFailure(t);
        verify(mView).onResponseFailure(t);
        verify(mView).hideProgress();
    }

    // Test using answer
    @Test
    public void requestDataFromServer_onFinished(){
        //pre
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ((OnFinishedListener)invocation.getArguments()[0]).onFinished(new ArrayList<NewsInfo>());
                return null;
            }
        }).when(mInteractor).getNewsArrayList(any(OnFinishedListener.class));

        //execution
        mUserActionListener.requestDataFromServer();

        //result verification
        verify(mView).showProgress();
        verify(mInteractor, times(1)).getNewsArrayList(any(OnFinishedListener.class));
        verify(mView).setDataToRecyclerView(anyList());
        verify(mView).hideProgress();
    }

    @Test
    public void requestDataFromServer_onFailure(){

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ((OnFinishedListener)invocation.getArguments()[0]).onFailure(new Exception("Some Exception"));
                return null;
            }
        }).when(mInteractor).getNewsArrayList(any(OnFinishedListener.class));
        mUserActionListener.requestDataFromServer();
        verify(mView).showProgress();
        verify(mInteractor).getNewsArrayList(any(OnFinishedListener.class));
        verify(mView).onResponseFailure(org.mockito.Matchers.<Throwable>anyObject());
        verify(mView).hideProgress();
    }

    @Test
    public void onNewsItemClick_showCommentScreen(){
        when(mNewsInfo.getKidsCount()).thenReturn(5L);

        mUserActionListener.onNewsItemClick(mNewsInfo);

        verify(mView).showCommentsScreen(mNewsInfo);
    }

    @Test
    public void onNewsItemClick_showNoComments(){
        when(mNewsInfo.getKidsCount()).thenReturn(0L);
        mUserActionListener.onNewsItemClick(mNewsInfo);
        verify(mView).showNoCommentsPopup();
    }

    @Test
    public void onNewsExternalLinkClick(){
        mUserActionListener.onNewsExternalLinkClick(any(String.class));
        verify(mView).openExternalLink(any(String.class));
    }

}
