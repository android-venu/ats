package com.hackernews.newslist.robo;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;

import com.google.common.collect.Lists;
import com.hackernews.BuildConfig;
import com.hackernews.R;
import com.hackernews.model.NewsInfo;
import com.hackernews.newslist.NewsListActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class NewsListActivityTest {

    @Test
    public void titleIsCorrect() throws Exception {
        Activity activity = Robolectric.setupActivity(NewsListActivity.class);
        Toolbar toolbar = activity.findViewById(R.id.toolbar);
        assertTrue(toolbar.getTitle().toString().equals("Hacker News"));
    }

    @Test
    public void verifyRecyclerViewVisibility() throws Exception {
        Activity activity = Robolectric.setupActivity(NewsListActivity.class);
        assertTrue(activity.findViewById(R.id.newsRecyclerView).isShown());
    }

}
