package com.hackernews.newslist.robo;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.common.collect.Lists;
import com.hackernews.BuildConfig;
import com.hackernews.R;
import com.hackernews.comments.CommentsActivity;
import com.hackernews.model.NewsInfo;
import com.hackernews.newslist.NewsListActivity;
import com.hackernews.newslist.NewsListAdapter;
import com.hackernews.newslist.NewsListItemClickListener;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class NewsListAdapterTest {
    private static List<NewsInfo> NEWS_LIST = Lists.newArrayList(new NewsInfo("Person1", "Title1", new long[]{1,2,3}),
            new NewsInfo("Person2", "Title2", new long[]{1,2,3}));
    private NewsListItemClickListener clickListener;
    private NewsListAdapter.NewsViewHolder holder;
    private NewsListAdapter adapter;
    private NewsListActivity activity;
    private View mockView;
    private Fragment context;


    @Before
    public void setUp() throws Exception {
        clickListener = mock(NewsListItemClickListener.class);
        adapter = new NewsListAdapter(NEWS_LIST, clickListener);
        activity = mock(NewsListActivity.class);
        mockView = mock(View.class);
        context = mock(Fragment.class);
//        stub(mockFragment.getString(anyInt())).toReturn("Candy");
    }
    @Test
    public void verifyItemCount() {
        assertTrue(adapter.getItemCount() == 2);
    }

    @Test
    public void onBindViewHolder_setsTextAndClickEvent() {
        LayoutInflater inflater = (LayoutInflater) RuntimeEnvironment.application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //We have a layout especially for the items in our recycler view. We will see it in a moment.
        View listItemView = inflater.inflate(R.layout.item_news, null, false);
        holder = new NewsListAdapter.NewsViewHolder(listItemView);
        adapter.onBindViewHolder(holder, 0);
        assertEquals("Title1",holder.titleTV.getText().toString());
        assertEquals("by Person1",holder.userNameTV.getText().toString());
    }

    @Test
    public void onCreateViewHolder_returnsNewNewsViewHolderOfCorrectLayout() {
        TestableNewsListAdapter testableAdapter = new TestableNewsListAdapter(NEWS_LIST, clickListener);
        testableAdapter.setMockView(mockView);
        NewsListAdapter.NewsViewHolder viewHolder = testableAdapter.onCreateViewHolder(new ConstraintLayout(RuntimeEnvironment.application), 0);
        Assertions.assertThat(viewHolder.itemView).isSameAs(mockView);

    }

    //Here we subclass and override the test subject again so we can use a mock view for testing, instead of the real one.
    static class TestableNewsListAdapter extends NewsListAdapter {
        public View mockView;

        public TestableNewsListAdapter(List<NewsInfo> newsList, NewsListItemClickListener itemClickListener) {
            super(newsList, itemClickListener);
        }

        public void setMockView(View mockView) {
            this.mockView = mockView;
        }

        @Override
        public View getLayout(ViewGroup parent) {
            return mockView;
        }
    }
}
