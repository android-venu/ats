package com.hackernews.comments.robo;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.collect.Lists;
import com.hackernews.BuildConfig;
import com.hackernews.R;
import com.hackernews.comments.CommentsListAdapter;
import com.hackernews.model.Comment;
import com.hackernews.model.NewsInfo;
import com.hackernews.newslist.NewsListActivity;
import com.hackernews.newslist.NewsListAdapter;
import com.hackernews.newslist.NewsListItemClickListener;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class CommentsAdapterTest {
    private static List<Comment> COMMENT_LIST = Lists.newArrayList(new Comment("Person1", "Comment1"),
            new Comment("Person2", "Comment2"));
    private CommentsListAdapter.CommentsViewHolder holder;
    private CommentsListAdapter adapter;
    private View mockView;

    @Before
    public void setUp() throws Exception {
        adapter = new CommentsListAdapter(COMMENT_LIST);
        mockView = mock(View.class);
    }
    @Test
    public void verifyItemCount() {
        assertTrue(adapter.getItemCount() == 2);
    }

    @Test
    public void onBindViewHolder_setsTextAndClickEvent() {
        LayoutInflater inflater = (LayoutInflater) RuntimeEnvironment.application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //We have a layout especially for the items in our recycler view. We will see it in a moment.
        View listItemView = inflater.inflate(R.layout.item_comment, null, false);
        holder = new CommentsListAdapter.CommentsViewHolder(listItemView);
        adapter.onBindViewHolder(holder, 0);
        assertEquals("Comment1",holder.commentTV.getText().toString());
        assertEquals("Person1",holder.userNameTV.getText().toString());
    }

    @Test
    public void onCreateViewHolder_returnsNewNewsViewHolderOfCorrectLayout() {
        TestableCommentsListAdapter testableAdapter = new TestableCommentsListAdapter(COMMENT_LIST);
        testableAdapter.setMockView(mockView);
        CommentsListAdapter.CommentsViewHolder viewHolder = testableAdapter.onCreateViewHolder(new ConstraintLayout(RuntimeEnvironment.application), 0);
        Assertions.assertThat(viewHolder.itemView).isSameAs(mockView);

    }

    //Here we subclass and override the test subject again so we can use a mock view for testing, instead of the real one.
    static class TestableCommentsListAdapter extends CommentsListAdapter {
        public View mockView;

        public TestableCommentsListAdapter(List<Comment> comments) {
            super(comments);
        }

        public void setMockView(View mockView) {
            this.mockView = mockView;
        }

        @Override
        public View getLayout(ViewGroup parent) {
            return mockView;
        }
    }
}
