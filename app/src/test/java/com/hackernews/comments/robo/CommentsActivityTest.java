package com.hackernews.comments.robo;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.Toolbar;

import com.hackernews.BuildConfig;
import com.hackernews.R;
import com.hackernews.comments.CommentsActivity;
import com.hackernews.newslist.NewsListActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class CommentsActivityTest {
    private CommentsActivity activity;

    @Before
    public void setUp() {
        Intent intent = new Intent();
        intent.putExtra(CommentsActivity.EXTRA_NEWS_TITLE, "Story1");
        intent.putExtra(CommentsActivity.EXTRA_COMMENT_IDS, new long[]{1,2,3});
        activity = Robolectric.buildActivity(CommentsActivity.class, intent).create().resume().get();
    }
    @Test
    public void titleIsCorrect() throws Exception {
        Toolbar toolbar = activity.findViewById(R.id.toolbar);
        assertTrue(toolbar.getTitle().toString().equals("Story1"));
    }

}
