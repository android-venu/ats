package com.hackernews.comments;

import com.hackernews.model.Comment;

import com.hackernews.comments.CommentsContract.CommentsInteractor;
import com.hackernews.comments.CommentsContract.UserActionsListener;
import com.hackernews.comments.CommentsContract.CommentsView;
import com.hackernews.comments.CommentsContract.CommentsInteractor.OnFinishedListener;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class CommentsPresenterTest {
    @Mock
    CommentsInteractor mInteractor;
    @Mock
    CommentsView mView;
    @Mock
    OnFinishedListener mListener;
    @Mock
    Comment mComment;

    UserActionsListener mUserActionListener;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mUserActionListener = new CommentsPresenter(mView, mInteractor);
    }

    @Test
    public void onDestroy() {
        mUserActionListener.onDestroy();
        assertEquals(true, mUserActionListener.isViewReleased());
    }

    @Test
    public void requestDataFromServer_onFinished(){
        long[] commentIds = new long[] {1,2};

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ((OnFinishedListener)invocation.getArguments()[1]).onFinished(new ArrayList<Comment>());
                return null;
            }
        }).when(mInteractor).getCommentsList(any(long[].class), any(OnFinishedListener.class));

        mUserActionListener.requestDataFromServer(commentIds);

        verify(mView).showProgress();
        verify(mInteractor, times(1)).getCommentsList(any(long[].class),
                any(OnFinishedListener.class));
        verify(mView).setDataToRecyclerView(anyList());
        verify(mView).hideProgress();
    }

    @Test
    public void requestDataFromServer_onFailure(){
        long[] commentIds = new long[] {1,2};
        ArgumentCaptor<long[]> argumentCaptor = ArgumentCaptor.forClass(long[].class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ((OnFinishedListener)invocation.getArguments()[1]).onFailure(new Exception("Some"));
                return null;
            }
        }).when(mInteractor).getCommentsList(any(long[].class), any(OnFinishedListener.class));

        mUserActionListener.requestDataFromServer(commentIds);

        verify(mView).showProgress();
        verify(mInteractor, times(1)).getCommentsList(argumentCaptor.capture(),
                any(OnFinishedListener.class));
        assertEquals(argumentCaptor.getValue(), commentIds);
        verify(mView).onResponseFailure(Matchers.<Throwable>any());
        verify(mView).hideProgress();
    }
}
