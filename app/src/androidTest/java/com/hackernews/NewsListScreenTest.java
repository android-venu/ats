package com.hackernews;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.hackernews.newslist.NewsListActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.endsWith;

/**
 * Tests for the news list screen, the main screen which contains a list of all news items.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class NewsListScreenTest {

    @Rule
    public ActivityTestRule<NewsListActivity> mNewsListActivityTestRule =
            new ActivityTestRule<>(NewsListActivity.class);

    @Test
    public void verifyNewsList() throws Exception {
        onView(withId(R.id.newsRecyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(2, click()));

        //onView(withText(endsWith(mNewsListActivityTestRule.getActivity().getString(R.string.title_hacker_news)))).check(matches(isDisplayed()));
//        onView(withId(R.id.toolbar)).check(matches(withText(R.string.title_hacker_news)));
//        // Check if the add note screen is displayed
//        onView(withId(R.id.progressBar)).check(matches(isDisplayed()));
//        onView(withId(R.id.newsRecyclerView)).check(matches(isDisplayed()));
    }
}
