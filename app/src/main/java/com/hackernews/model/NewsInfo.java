package com.hackernews.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsInfo implements Parcelable {

    @SerializedName("by")
    @Expose
    private String by;
    @SerializedName("descendants")
    @Expose
    private long descendants;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("kids")
    @Expose
    private long[] kids = null;
    @SerializedName("score")
    @Expose
    private long score;
    @SerializedName("time")
    @Expose
    private long time;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("url")
    @Expose
    private String url;
    public final static Creator<NewsInfo> CREATOR = new Creator<NewsInfo>() {

        public NewsInfo createFromParcel(Parcel in) {
            return new NewsInfo(in);
        }

        public NewsInfo[] newArray(int size) {
            return (new NewsInfo[size]);
        }

    };

    protected NewsInfo(Parcel in) {
        this.by = ((String) in.readValue((String.class.getClassLoader())));
        this.descendants = ((long) in.readValue((long.class.getClassLoader())));
        this.id = ((long) in.readValue((long.class.getClassLoader())));
        in.readLongArray(this.kids);
        this.score = ((long) in.readValue((long.class.getClassLoader())));
        this.time = ((long) in.readValue((long.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.url = ((String) in.readValue((String.class.getClassLoader())));
    }

    public NewsInfo() {
    }

    public NewsInfo(String by, String title, long[] kids) {
        this.by = by;
        this.title = title;
        this.kids = kids;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public long getDescendants() {
        return descendants;
    }

    public void setDescendants(long descendants) {
        this.descendants = descendants;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long[] getKids() {
        return kids;
    }

    public void setKids(long[] kids) {
        this.kids = kids;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getKidsCount() {
        return kids == null ? 0 :kids.length;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(by);
        dest.writeValue(descendants);
        dest.writeValue(id);
        dest.writeLongArray(kids);
        dest.writeValue(score);
        dest.writeValue(time);
        dest.writeValue(title);
        dest.writeValue(type);
        dest.writeValue(url);
    }

    public int describeContents() {
        return 0;
    }

}
