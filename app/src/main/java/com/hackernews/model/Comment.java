
package com.hackernews.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comment implements Parcelable
{
    @SerializedName("by")
    @Expose
    private String by;
    @SerializedName("dead")
    @Expose
    private boolean dead;
    @SerializedName("deleted")
    @Expose
    private boolean deleted;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("parent")
    @Expose
    private long parent;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("time")
    @Expose
    private long time;
    @SerializedName("type")
    @Expose
    private String type;
    public final static Creator<Comment> CREATOR = new Creator<Comment>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        public Comment[] newArray(int size) {
            return (new Comment[size]);
        }
    };

    protected Comment(Parcel in) {
        this.by = ((String) in.readValue((String.class.getClassLoader())));
        this.dead = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.deleted = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.id = ((long) in.readValue((long.class.getClassLoader())));
        this.parent = ((long) in.readValue((long.class.getClassLoader())));
        this.text = ((String) in.readValue((String.class.getClassLoader())));
        this.time = ((long) in.readValue((long.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Comment(String by, String text) {
        this.by = by;
        this.text = text;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getParent() {
        return parent;
    }

    public void setParent(long parent) {
        this.parent = parent;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(by);
        dest.writeValue(dead);
        dest.writeValue(deleted);
        dest.writeValue(id);
        dest.writeValue(parent);
        dest.writeValue(text);
        dest.writeValue(time);
        dest.writeValue(type);
    }

    public int describeContents() {
        return  0;
    }

}
