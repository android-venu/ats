package com.hackernews.comments;

import com.hackernews.model.Comment;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class CommentsPresenter implements CommentsContract.UserActionsListener, CommentsContract.CommentsInteractor.OnFinishedListener {
    private WeakReference<CommentsContract.CommentsView> mView;
    private WeakReference<CommentsContract.CommentsInteractor> mInteractor;

    public CommentsPresenter(CommentsContract.CommentsView view, CommentsContract.CommentsInteractor interactor) {
        mView = new WeakReference<>(view);
        mInteractor = new WeakReference<>(interactor);
    }

    @Override
    public void onDestroy() {
        mView.clear();
    }

    @Override
    public void requestDataFromServer(long[] ids) {
        if (mView.get() != null) {
            mView.get().showProgress();
        }
        if (mInteractor.get() != null) {
            mInteractor.get().getCommentsList(ids, this);
        }
    }

    @Override
    public void onFinished(List<Comment> comments) {
        if (mView.get() != null) {
            mView.get().setDataToRecyclerView(comments);
            mView.get().hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mView != null) {
            mView.get().onResponseFailure(t);
            mView.get().hideProgress();
        }
    }

    @Override
    public boolean isViewReleased() {
        return mView == null || mView.get() == null;
    }
}
