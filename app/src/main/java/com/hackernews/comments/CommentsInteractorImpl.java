package com.hackernews.comments;


import com.hackernews.model.Comment;
import com.hackernews.network.HackerNewsApiService;
import com.hackernews.network.NetworkManager;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class CommentsInteractorImpl implements CommentsContract.CommentsInteractor {

    @Override
    public void getCommentsList(final long[]commentIds,final OnFinishedListener onFinishedListener) {
        final HackerNewsApiService service = NetworkManager.createService(HackerNewsApiService.class);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    ArrayList<Comment> commentList = new ArrayList<Comment>();
                    for (long id: commentIds){
                        try {
                            Call<Comment> commentCall = service.getComment(id);
                            Response<Comment> commentResponse = commentCall.execute();
                            if (commentResponse!=null && commentResponse.isSuccessful() && commentResponse.body()!=null) {
                                Comment comment = commentResponse.body();
                                if (!comment.isDead() && !comment.isDeleted()) {
                                    commentList.add(comment);
                                }
                            }
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                    }
                    onFinishedListener.onFinished(commentList);
                } catch (Exception e) {
                    e.printStackTrace();
                    onFinishedListener.onFailure(e);
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }
}
