package com.hackernews.comments;

import com.hackernews.model.Comment;

import java.util.List;

public class CommentsContract {
    /**
     * Call when user interact with the news list view
     * */
    public interface UserActionsListener {
        void onDestroy();
        void requestDataFromServer(long[] ids);
        boolean isViewReleased();
    }

    public interface CommentsView {
        void showProgress();
        void hideProgress();
        void setDataToRecyclerView(List<Comment> comments);
        void onResponseFailure(Throwable t);
    }

    /**
     * This intractor is used to fetch data from web services.
     **/
    public interface CommentsInteractor {
        interface OnFinishedListener {
            void onFinished(List<Comment> comments);
            void onFailure(Throwable t);
        }
        void getCommentsList(long[] commentIds, OnFinishedListener onFinishedListener);
    }
}
