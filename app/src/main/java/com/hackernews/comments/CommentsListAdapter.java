package com.hackernews.comments;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.hackernews.R;
import com.hackernews.model.Comment;

import java.util.List;

public class CommentsListAdapter extends RecyclerView.Adapter<CommentsListAdapter.CommentsViewHolder>{
    private List<Comment> mCommentsList;
    public CommentsListAdapter(List<Comment> comments) {
        mCommentsList = comments;
    }

    @NonNull
    @Override
    public CommentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = getLayout(parent);
        return new CommentsViewHolder(view);
    }

    public View getLayout(ViewGroup parent) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsViewHolder holder, int position) {
        final Comment comment = mCommentsList.get(position);
        if(!TextUtils.isEmpty(comment.getText()))
            holder.commentTV.setText(Html.fromHtml(comment.getText()));
        else
            holder.commentTV.setText("");

        holder.userNameTV.setText(comment.getBy());
        CharSequence relativeTime = DateUtils.getRelativeTimeSpanString(comment.getTime()*1000);
        holder.timeTV.setText(relativeTime);
    }

    @Override
    public int getItemCount() {
        return mCommentsList == null ? 0 : mCommentsList.size();
    }

    public static class CommentsViewHolder extends RecyclerView.ViewHolder {
        public TextView userNameTV, timeTV, commentTV;

        public CommentsViewHolder(View itemView) {
            super(itemView);
            userNameTV =  itemView.findViewById(R.id.usernameTV);
            timeTV =  itemView.findViewById(R.id.timeTV);
            commentTV =  itemView.findViewById(R.id.commentTV);
        }
    }
}
