package com.hackernews.comments;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.hackernews.R;
import com.hackernews.model.Comment;
import com.hackernews.util.LoaderDialogFragment;

import java.util.List;

public class CommentsActivity extends AppCompatActivity implements CommentsContract.CommentsView {
    public static final String EXTRA_COMMENT_IDS = "EXTRA_COMMENT_IDS";
    public static final String EXTRA_NEWS_TITLE = "EXTRA_NEWS_TITLE";
    private long[] mCommentIds = null;
    private View mCoordinatorLayout;
    private RecyclerView mRecyclerView;
    private LoaderDialogFragment mLoaderFragment;
    private CommentsContract.UserActionsListener mUserActionListener;
    private String mNewsTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        mCommentIds = getIntent().getLongArrayExtra(EXTRA_COMMENT_IDS);
        mNewsTitle =  getIntent().getStringExtra(EXTRA_NEWS_TITLE);
        initUI();
        mUserActionListener = new CommentsPresenter(this, new CommentsInteractorImpl());
        mUserActionListener.requestDataFromServer(mCommentIds);
    }

    private void initUI() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mNewsTitle);

        mCoordinatorLayout = findViewById(R.id.coordinateLayout);
        mRecyclerView = findViewById(R.id.commentsRecyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUserActionListener.onDestroy();
    }


    public void showProgress() {
        if (mLoaderFragment==null || mLoaderFragment.isHidden()) {
            mLoaderFragment = new LoaderDialogFragment();
            mLoaderFragment.show(getFragmentManager(), LoaderDialogFragment.TAG);
        }
    }

    public void hideProgress() {
        try {
            if (mLoaderFragment != null && !mLoaderFragment.isHidden()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mLoaderFragment.dismiss();
                        mLoaderFragment = null;
                    }
                });
            }
        } catch (Exception e){
            Log.e("BaseActivity","hideLoader",e);
        }
    }

    @Override
    public void setDataToRecyclerView(final List<Comment> comments) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                CommentsListAdapter adapter = new CommentsListAdapter(comments);
                mRecyclerView.setAdapter(adapter);
            }
        });
    }

    @Override
    public void onResponseFailure(final Throwable t) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Snackbar snackbar = Snackbar.make(mCoordinatorLayout, "Something went wrong...Error message: " + t.getLocalizedMessage(), Snackbar.LENGTH_LONG);
                snackbar.setAction(R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });
                snackbar.show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
