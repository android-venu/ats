package com.hackernews.newslist;

import com.hackernews.model.NewsInfo;

import java.util.ArrayList;
import java.util.List;

public class NewsListContract {
    /**
     * Call when user interact with the news list view
     * */
    public interface UserActionsListener {
        void onDestroy();
        void onPullDownToRefresh();
        void requestDataFromServer();
        void onNewsItemClick(NewsInfo newsInfo);
        void onNewsExternalLinkClick(String url);
        boolean isViewReleased();
    }

    public interface NewsListView {
        void showProgress();
        void hideProgress();
        void setDataToRecyclerView(List<NewsInfo> newsList);
        void onResponseFailure(Throwable t);
        void showCommentsScreen(NewsInfo newsInfo);
        void showNoCommentsPopup();
        void openExternalLink(String url);
    }

    /**
     * This intractor is used to fetch data from web services.
     **/
    public interface NewsInteractor {

        interface OnFinishedListener {
            void onFinished(List<NewsInfo> newsList);
            void onFailure(Throwable t);
        }

        void getNewsArrayList(OnFinishedListener onFinishedListener);
    }


}
