package com.hackernews.newslist;

import com.hackernews.model.NewsInfo;
import com.hackernews.network.HackerNewsApiService;
import com.hackernews.network.NetworkManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class NewsInteractorImpl implements NewsListContract.NewsInteractor {

    @Override
    public void getNewsArrayList(final OnFinishedListener onFinishedListener) {
        final HackerNewsApiService service = NetworkManager.createService(HackerNewsApiService.class);

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Call<List<Long>> call = service.getTopStories();
                try {
                    Response<List<Long>> storiesResponse = call.execute();
                    if (storiesResponse!=null && storiesResponse.isSuccessful() && storiesResponse.body()!=null){
                        List<Long> newsIds = storiesResponse.body();
                        ArrayList<NewsInfo> newsList = new ArrayList<NewsInfo>();
                        for (int i=0; i< 20;i++){
                            try {
                                Call<NewsInfo> newsInfoCall = service.getNewsInfo1(newsIds.get(i));
                                Response<NewsInfo> newsInfoResponse = newsInfoCall.execute();
                                if (newsInfoResponse!=null && newsInfoResponse.isSuccessful() && newsInfoResponse.body()!=null) {
                                    NewsInfo newsInfo = newsInfoResponse.body();
                                    newsList.add(newsInfo);
                                }
                            }catch (IOException e){
                                e.printStackTrace();
                            }
                        }
                        onFinishedListener.onFinished(newsList);
                    } else {
                        onFinishedListener.onFailure(new Exception(storiesResponse.errorBody().string()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    onFinishedListener.onFailure(e);
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }
}
