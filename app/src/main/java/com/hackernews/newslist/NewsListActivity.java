package com.hackernews.newslist;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.VisibleForTesting;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.IdlingResource;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.hackernews.comments.CommentsActivity;
import com.hackernews.util.LoaderDialogFragment;
import com.hackernews.R;
import com.hackernews.model.NewsInfo;

import java.net.UnknownHostException;
import java.util.List;

public class NewsListActivity extends AppCompatActivity implements NewsListContract.NewsListView {
    private RecyclerView mRecyclerView;
    private CoordinatorLayout mCoordinatorLayout;
    private LoaderDialogFragment mLoaderFragment;
    private NewsListContract.UserActionsListener mUserActionListener;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_list);
        initUI();

        mUserActionListener = new NewsListPresenter(this, new NewsInteractorImpl());
        mUserActionListener.requestDataFromServer();
    }

    private void initUI(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mCoordinatorLayout = findViewById(R.id.coordinateLayout);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mRecyclerView = findViewById(R.id.newsRecyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
                mUserActionListener.onPullDownToRefresh();
            }
        });
    }

    private NewsListItemClickListener newsListItemClickListener = new NewsListItemClickListener() {
        @Override
        public void onItemClick(NewsInfo newsInfo) {
            mUserActionListener.onNewsItemClick(newsInfo);
        }

        @Override
        public void onExternalLinkClick(String url) {
            mUserActionListener.onNewsExternalLinkClick(url);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUserActionListener.onDestroy();
    }

    @Override
    public void showProgress() {
        if (mLoaderFragment==null || mLoaderFragment.isHidden()) {
            mLoaderFragment = new LoaderDialogFragment();
            mLoaderFragment.show(getFragmentManager(), LoaderDialogFragment.TAG);
        }
    }

    @Override
    public void hideProgress() {
        try {
            if (mLoaderFragment != null && !mLoaderFragment.isHidden()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mLoaderFragment.dismiss();
                        mLoaderFragment = null;
                    }
                });
            }
        } catch (Exception e){
            Log.e("BaseActivity","hideLoader",e);
        }
    }

    @Override
    public void setDataToRecyclerView(final List<NewsInfo> newsList) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                NewsListAdapter adapter = new NewsListAdapter(newsList, newsListItemClickListener);
                mRecyclerView.setAdapter(adapter);
            }
        });
    }

    @Override
    public void onResponseFailure(final Throwable t) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(t instanceof UnknownHostException){
                    Toast.makeText(getApplicationContext(), getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                }else {
                    final Snackbar snackbar = Snackbar.make(mCoordinatorLayout, "Something went wrong...Error message: " + t.getLocalizedMessage(), Snackbar.LENGTH_LONG);
                    snackbar.setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            snackbar.dismiss();
                        }
                    });
                    snackbar.show();
                }
            }
        });
    }

    @Override
    public void showCommentsScreen(NewsInfo newsInfo) {
        Intent intent = new Intent(this, CommentsActivity.class);
        long[] commentIds = new long[newsInfo.getKids().length];
        for (int i=0; i< newsInfo.getKids().length; i++){
            commentIds[i] = newsInfo.getKids()[i];
        }
        intent.putExtra(CommentsActivity.EXTRA_COMMENT_IDS, commentIds);
        intent.putExtra(CommentsActivity.EXTRA_NEWS_TITLE, newsInfo.getTitle());
        startActivity(intent);
    }

    @Override
    public void showNoCommentsPopup() {
        final Snackbar snackbar = Snackbar.make(mCoordinatorLayout, "Comments are not available for this story", Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.ok, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Override
    public void openExternalLink(String url) {
        CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                .addDefaultShareMenuItem()
                .setToolbarColor(this.getResources()
                        .getColor(R.color.colorPrimary))
                .setShowTitle(true)
                //.setCloseButtonIcon(getDrawable(R.drawable.ic_close))
                .build();
        if(url!=null)customTabsIntent.launchUrl(this, Uri.parse(url));
    }
}
