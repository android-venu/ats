package com.hackernews.newslist;

import com.hackernews.model.NewsInfo;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class NewsListPresenter implements NewsListContract.UserActionsListener, NewsListContract.NewsInteractor.OnFinishedListener {
    private WeakReference<NewsListContract.NewsListView> mView;
    private WeakReference<NewsListContract.NewsInteractor> mInteractor;

    public NewsListPresenter(NewsListContract.NewsListView view, NewsListContract.NewsInteractor interactor) {
        mView = new WeakReference<>(view);
        mInteractor = new WeakReference<>(interactor);
    }

    @Override
    public void onDestroy() {
        mView.clear();
    }

    @Override
    public void onPullDownToRefresh() {
        if (mView.get() != null) {
            mView.get().showProgress();
        }
        if (mInteractor.get() != null) {
            mInteractor.get().getNewsArrayList(this);
        }
    }

    @Override
    public void requestDataFromServer() {
        if (mView.get() != null) {
            mView.get().showProgress();
        }
        if (mInteractor.get() != null) {
            mInteractor.get().getNewsArrayList(this);
        }
    }

    @Override
    public void onNewsItemClick(NewsInfo newsInfo) {
        if (mView.get() != null) {
            if (newsInfo.getKidsCount() > 0) {
                mView.get().showCommentsScreen(newsInfo);
            } else {
                mView.get().showNoCommentsPopup();
            }
        }
    }

    @Override
    public void onNewsExternalLinkClick(String url) {
        if (mView.get() != null) {
            mView.get().openExternalLink(url);
        }
    }

    @Override
    public void onFinished(List<NewsInfo> newsList) {
        if (mView.get() != null) {
            mView.get().setDataToRecyclerView(newsList);
            mView.get().hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (mView != null) {
            mView.get().onResponseFailure(t);
            mView.get().hideProgress();
        }
    }

    @Override
    public boolean isViewReleased() {
        return mView == null || mView.get() == null;
    }
}
