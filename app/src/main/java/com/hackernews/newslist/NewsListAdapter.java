package com.hackernews.newslist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hackernews.R;
import com.hackernews.model.NewsInfo;

import java.util.ArrayList;
import java.util.List;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.NewsViewHolder>{
    private List<NewsInfo> mNewsList;
    private NewsListItemClickListener mItemClickListener;
    public NewsListAdapter(List<NewsInfo> newsList, NewsListItemClickListener itemClickListener) {
        mNewsList = newsList;
        mItemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = getLayout(parent);
        return new NewsViewHolder(view);
    }

    public View getLayout(ViewGroup parent) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false);
    }

    public NewsInfo getItemAtPosition(int position) {
        return mNewsList.get(position);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        Context context = holder.itemView.getContext();
        final NewsInfo newsInfo = getItemAtPosition(position);
        holder.titleTV.setText(newsInfo.getTitle());
        holder.pointsTV.setText(context.getResources().getString(R.string.points_count,newsInfo.getScore()));
        holder.userNameTV.setText(context.getResources().getString(R.string.news_item_username,newsInfo.getBy()));
        holder.commentsCountTV.setText(context.getResources().getString(R.string.comments_count,newsInfo.getKidsCount()));
        CharSequence relativeTime = DateUtils.getRelativeTimeSpanString(newsInfo.getTime()*1000);
        holder.timeTV.setText(relativeTime);
        holder.openLinkImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener!=null) {
                    mItemClickListener.onExternalLinkClick(newsInfo.getUrl());
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener!=null) {
                    mItemClickListener.onItemClick(newsInfo);
                }
            }
        });
    }



    @Override
    public int getItemCount() {
        return mNewsList == null ? 0 : mNewsList.size();
    }

    public static class NewsViewHolder extends RecyclerView.ViewHolder {
        public TextView titleTV, pointsTV, userNameTV, timeTV, commentsCountTV;
        public ImageView openLinkImageView;

        public NewsViewHolder(View itemView) {
            super(itemView);
            titleTV =  itemView.findViewById(R.id.titleTV);
            openLinkImageView = itemView.findViewById(R.id.openLinkImage);
            pointsTV =  itemView.findViewById(R.id.pointsTV);
            userNameTV =  itemView.findViewById(R.id.userNameTV);
            timeTV =  itemView.findViewById(R.id.timeTV);
            commentsCountTV =  itemView.findViewById(R.id.commentsCountTV);
        }
    }
}
