package com.hackernews.newslist;

import com.hackernews.model.NewsInfo;

public interface NewsListItemClickListener {
    void onItemClick(NewsInfo newsInfo);
    void onExternalLinkClick(String url);
}
