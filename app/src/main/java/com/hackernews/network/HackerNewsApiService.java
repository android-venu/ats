package com.hackernews.network;

import com.hackernews.model.Comment;
import com.hackernews.model.NewsInfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface HackerNewsApiService {

    @GET("/v0/topstories.json")
    Call<List<Long>> getTopStories();

    @GET("/v0/item/{newsId}.json")
    Call<NewsInfo> getNewsInfo1(@Path("newsId") Long newsId);

    @GET("/v0/item/{commentID}.json")
    Call<Comment> getComment(@Path("commentID") Long commentID);

}
